package com.cartao.services;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import com.cartao.models.dtos.Extrato;
import com.cartao.models.dtos.PagamentoDTO;
import com.cartao.repositories.CartaoRepository;
import com.cartao.repositories.PagamentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoServices {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoServices cartaoServices;

    @Autowired
    CartaoRepository cartaoRepository;


    public List<Extrato> consultarPagamentoPorCartao(Cartao cartao) throws ObjectNotFoundException {
        List<Extrato> extratoLista = new ArrayList<>();
        List<Pagamento> pagamentoLista = pagamentoRepository.findByCartao(cartao);
        if (pagamentoLista.size() > 0) {
            for (Pagamento pagamento : pagamentoLista) {
                Extrato extrato = new Extrato();
                extrato.setValor(pagamento.getValor());
                extrato.setDescricao(pagamento.getDescricao());
                extrato.setCartao_id(pagamento.getCartao().getId());
                extrato.setId(pagamento.getId());
                extratoLista.add(extrato);
            }
            return extratoLista;
        }
            throw new ObjectNotFoundException(Cartao.class, "Pagamentos para este cartão não encontrados");
        }

    public Pagamento salvarPagamento(PagamentoDTO pagamentoDTO) throws ObjectNotFoundException {
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(pagamentoDTO.getCartao_id());
        /** variavel precisa ser sempre minusculo senao nao funciona **/
        if (cartaoOptional.isPresent() && cartaoOptional.get().isAtivo() ) {
            Pagamento pagamento = new Pagamento();
            pagamento.setCartao(cartaoOptional.get());
            pagamento.setValor(pagamentoDTO.getValor());
            pagamento.setDescricao(pagamentoDTO.getDescricao());

            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }
        throw new ObjectNotFoundException(Pagamento.class, "Cartão não existe ou não está ativo");
    }
}
