package com.cartao.services;

import com.cartao.models.Cartao;
import com.cartao.models.Cliente;
import com.cartao.models.dtos.CartaoDTO;
import com.cartao.models.dtos.RespostaCartaoDTO;
import com.cartao.repositories.CartaoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartaoServices {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteServices clienteServices;


    public Iterable<Cartao> buscarTodosCartoes(List<Long> cartaoId) {
        Iterable<Cartao> cartaoIterable = cartaoRepository.findAllById(cartaoId);
        return cartaoIterable;
    }

    public Iterable<Cartao> buscarTodosCartoes() {
        Iterable<Cartao> cartao = cartaoRepository.findAll();
        return cartao;
    }

    public Optional<Cartao> buscarPorId(Long id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        return cartaoOptional;
    }


    public Optional<Cartao> buscarPorNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        return cartaoOptional;
    }


    public RespostaCartaoDTO salvarCartao(CartaoDTO cartaoDTO) throws ObjectNotFoundException {
        Optional<Cliente> clienteOptional = clienteServices.buscarPorId(cartaoDTO.getClienteId());

        if (clienteOptional.isPresent()) {
            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setCliente(clienteOptional.get());
            cartao.setAtivo(false);

            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            RespostaCartaoDTO respostaCartao = new RespostaCartaoDTO();
            respostaCartao.setId(cartaoObjeto.getId());
            respostaCartao.setNumero(cartaoObjeto.getNumero());
            respostaCartao.setClienteId(cartaoObjeto.getCliente().getId());
            respostaCartao.setAtivo(cartaoObjeto.isAtivo());

            return respostaCartao;
        }
        throw new ObjectNotFoundException(Cartao.class, "Cliente não cadastrado");
    }

    public RespostaCartaoDTO atualizarCartao(Cartao cartao) throws ObjectNotFoundException {
        Optional<Cartao> cartaoOptional = buscarPorNumero(cartao.getNumero());

        if (cartaoOptional.isPresent()) {

            Cartao cartaoData = cartaoOptional.get();
            if (cartao.isAtivo()) { cartaoData.setAtivo(cartao.isAtivo()); } else {cartaoData.setAtivo(false);}

            if (cartao.getNumero() != null) {
                cartaoData.setNumero(cartao.getNumero());
            }

            if ( cartao.getCliente() != null ) {
            if (cartao.getCliente().getId() != null) {
                cartaoData.getCliente().setId(cartao.getCliente().getId());
            }

            if (cartao.getCliente().getNome() != null) {
                cartaoData.getCliente().setNome(cartao.getCliente().getNome());
            }

            }

            Cartao cartaoObjeto = cartaoRepository.save(cartaoData);
            RespostaCartaoDTO respostaCartao = new RespostaCartaoDTO();
            respostaCartao.setId(cartaoObjeto.getId());
            respostaCartao.setNumero(cartaoObjeto.getNumero());
            respostaCartao.setClienteId(cartaoObjeto.getCliente().getId());
            if (cartaoData.isAtivo()) {respostaCartao.setAtivo(true);} else {respostaCartao.setAtivo(false);}
            return respostaCartao;
        }
        throw new ObjectNotFoundException(Cartao.class, "Cartao não cadastrado");
    }

    public void deletarCartao(Cartao cartao) {
        cartaoRepository.delete(cartao);
    }
}