package com.cartao.controllers;

import com.cartao.models.Cartao;
import com.cartao.models.dtos.CartaoDTO;
import com.cartao.models.dtos.RespostaCartaoDTO;
import com.cartao.models.dtos.RespostaCartaoGet;
import com.cartao.models.dtos.UpdateCartaoRequest;
import com.cartao.services.CartaoServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoServices cartaoServices;

    @GetMapping
    public Iterable<Cartao> buscarTodosCartoes(){
        return cartaoServices.buscarTodosCartoes();
    }

    /*@GetMapping("/{id}")
    public ResponseEntity<Cartao> buscarCartao(@PathVariable Long id){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(id);
        if (cartaoOptional.isPresent()){
            return ResponseEntity.status(200).body(cartaoOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }*/

    @GetMapping("/{numero}")
    public ResponseEntity<RespostaCartaoGet> buscarCartao(@PathVariable String numero){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorNumero(numero);
        if (cartaoOptional.isPresent()){
            RespostaCartaoGet respostaGET = new RespostaCartaoGet();
            respostaGET.setId(cartaoOptional.get().getId());
            respostaGET.setNumero(cartaoOptional.get().getNumero());
            respostaGET.setClienteId(cartaoOptional.get().getCliente().getId());
            return ResponseEntity.status(200).body(respostaGET);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<RespostaCartaoDTO> incluirCartao(@RequestBody @Valid CartaoDTO cartaoDTO) {
        RespostaCartaoDTO cartaoObjeto = cartaoServices.salvarCartao(cartaoDTO);
        return ResponseEntity.status(201).body(cartaoObjeto);
    }

    @PutMapping("/{numero}")
    public ResponseEntity<RespostaCartaoDTO> atualizarCartao(@PathVariable String numero, @RequestBody @Valid Cartao cartao){
        cartao.setNumero(numero);
        RespostaCartaoDTO cartaoObjetoR;
        try{ cartaoObjetoR = cartaoServices.atualizarCartao(cartao);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return ResponseEntity.status(200).body(cartaoObjetoR);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<RespostaCartaoDTO> ativarCartao(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest ){
        Cartao cartaoObjeto = new Cartao();
        RespostaCartaoDTO cartaoDTOResposta;
        cartaoObjeto.setNumero(numero);
        cartaoObjeto.setAtivo(updateCartaoRequest.getAtivo());

        try{ cartaoDTOResposta = cartaoServices.atualizarCartao(cartaoObjeto); }
            catch (ObjectNotFoundException e){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
            }
        return ResponseEntity.status(200).body(cartaoDTOResposta);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Cartao> deletarCartao(@PathVariable Long id){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(id);
        if (cartaoOptional.isPresent()) {
            cartaoServices.deletarCartao(cartaoOptional.get());
            return ResponseEntity.status(204).body(cartaoOptional.get());
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}
