package com.cartao.controllers;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import com.cartao.models.dtos.Extrato;
import com.cartao.models.dtos.PagamentoDTO;
import com.cartao.models.dtos.RespostaPagamentoPost;
import com.cartao.services.CartaoServices;
import com.cartao.services.PagamentoServices;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoServices pagamentoServices;

    @Autowired
    private CartaoServices cartaoServices;

    @GetMapping("/{id}")
    public ResponseEntity<List<Extrato>> buscarPagamento(@PathVariable Long id){
        Optional<Cartao> cartaoOptional = cartaoServices.buscarPorId(id);

        Cartao cartao = new Cartao();
        cartao.setId(cartaoOptional.get().getId());
        cartao.setCliente(cartaoOptional.get().getCliente());
        cartao.setNumero(cartaoOptional.get().getNumero());
        cartao.setAtivo(true);

        List<Extrato> extratoLista;
        try{extratoLista = pagamentoServices.consultarPagamentoPorCartao(cartao);}
        catch (ObjectNotFoundException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return ResponseEntity.status(200).body(extratoLista);
    }

    @PostMapping
    public ResponseEntity<RespostaPagamentoPost> incluirPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
        Pagamento pagamentoObjeto = pagamentoServices.salvarPagamento(pagamentoDTO);
        RespostaPagamentoPost resposta = new RespostaPagamentoPost();
        resposta.setId(pagamentoObjeto.getId());
        resposta.setCartao_id(pagamentoObjeto.getCartao().getId());
        resposta.setDescricao(pagamentoObjeto.getDescricao());
        resposta.setValor(pagamentoObjeto.getValor());
        return ResponseEntity.status(201).body(resposta);
    }
}