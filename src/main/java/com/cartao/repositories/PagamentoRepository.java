package com.cartao.repositories;

import com.cartao.models.Cartao;
import com.cartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findByCartao(Cartao cartao);

}