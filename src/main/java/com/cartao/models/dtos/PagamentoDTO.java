package com.cartao.models.dtos;

public class PagamentoDTO {
    private Long cartao_id;
    private String descricao;
    private double valor;

    public PagamentoDTO() {
    }

    public PagamentoDTO(Long cartao_id, String descricao, double valor) {
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Long cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
